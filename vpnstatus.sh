#/bin/bash

###VPN status information###

if ifconfig | grep -q 'tun0'; then
   echo "VPN:UP"
else
   echo "VPN:DOWN"
   #echo -e $'\e[1;31m' VPN:DOWN
fi
